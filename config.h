#include <string.h>
#include <stdio.h>
#include <stdlib.h>
//change increase/decrease interval
char *upinterval = "10";
char *downinterval = "10";
//change the seek interval
char *seekforwardint = "10";
char *seekbackint = "10";

//keybinds//
const char skipsong = 'n';
const char prevsong = 'p';
const char toggle = ' ';
const char clearconsole = 'c';
const char search_play = 's';
const char nowplaying = 't';
const char fastforward = 'f';
const char fastbackward = 'b';
const char list_title = 'l';
const char play_pos = 'z';
const char list_playlist = 'd';
const char enable_raw = 'r';
//change the prompt
char cprompt[1024] = "\nSMP console ~$ ";

