![Alt text](smp.png?raw=true "Simple Music Player")
# Simple MPD player
## a Simple MPD player written in C

### features:
- customizable keybinds (via config.h)
- minimalistic (no more than 100 LOC)
- easy to understand (the code is simple)
- has all the basic features (pause/unpause skip, play previous song, vol up/down)

## contributing
### feel free to contribute and modify this as you wish.

## installation
### running the install script
#### to install the program, the folllowing commands must be run
#### first, make sure the script is executable
```chmod +x install.sh```
#### then, execute the scirpt
``` ./install.sh```

## dependencies
- mpc
- mpd

